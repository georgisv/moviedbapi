﻿namespace MovieDbAPI.Data
{
    using Microsoft.EntityFrameworkCore;
    using MovieDbAPI.Data.DbModels;

    public class ApplicationDbContext : DbContext
    {
        public ApplicationDbContext(DbContextOptions<ApplicationDbContext> options)
           : base(options)
        {
        }

        public DbSet<User> Users { get; set; }

        public DbSet<Favourite> Favourites { get; set; }

        public DbSet<UserFavourites> UserFavourites { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            base.OnConfiguring(optionsBuilder);
        }

        protected override void OnModelCreating(ModelBuilder builder)
        {
            base.OnModelCreating(builder);

            builder.Entity<UserFavourites>()
                .HasKey(uf => new { uf.FavouriteId, uf.UserId });

            builder.Entity<UserFavourites>()
                .HasOne(uf => uf.Favourite)
                .WithMany(f => f.UserFavourites)
                .HasForeignKey(uf => uf.FavouriteId);

            builder.Entity<UserFavourites>()
                .HasOne(sc => sc.User)
                .WithMany(u => u.UserFavourites)
                .HasForeignKey(sc => sc.UserId);
        }
    }
}
