﻿namespace MovieDbAPI.Data.DbModels
{
    public class UserFavourites 
    {
        public User User { get; set; }

        public int UserId { get; set; }

        public Favourite Favourite { get; set; }

        public int FavouriteId { get; set; }
    }
}
