﻿namespace MovieDbAPI.Data.DbModels
{
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;

    public class Favourite
    {
        public Favourite()
        {
            this.UserFavourites = new List<UserFavourites>();
        }

        public int Id { get; set; }

        [Required]
        public string MovieId { get; set; }

        [Required]
        [MaxLength(100)]
        public string MovieName { get; set; }

        [Required]
        [Url]
        public string MoviePictureUrl { get; set; }

        public ICollection<UserFavourites> UserFavourites { get; set; }
    }
}
