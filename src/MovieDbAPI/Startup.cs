namespace MovieDbAPI
{
    using Microsoft.AspNetCore.Builder;
    using Microsoft.AspNetCore.Hosting;
    using Microsoft.EntityFrameworkCore;
    using Microsoft.Extensions.Configuration;
    using Microsoft.Extensions.DependencyInjection;
    using Microsoft.Extensions.Hosting;
    using Microsoft.Net.Http.Headers;
    using MovieDbAPI.Data;
    using MovieDbAPI.Models.ConfigModels;
    using MovieDbAPI.Services;
    using System;
    using System.Linq;

    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        public void ConfigureServices(IServiceCollection services)
        {
            services.AddDbContext<ApplicationDbContext>(
                    options => options.UseSqlServer(Configuration.GetConnectionString("DefaultConnection")));

            var movieDbCOnfig = Configuration.GetSection("MoviesDbConfig").Get<MovieDbConfig>();

            services.AddControllers();
            services.AddHttpClient("MovieDb", httpClient =>
            {
                httpClient.BaseAddress = new Uri(movieDbCOnfig.BaseUrl);
            });

            services.AddSwaggerGen(c =>
            {
                c.ResolveConflictingActions(apiDescriptions => apiDescriptions.First());
            });

            services.AddSingleton(movieDbCOnfig);
            services.AddTransient<UsersService>();
            services.AddTransient<MovieService>();
        }

        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            app.UseHttpsRedirection();
            app.UseRouting();
            app.UseAuthorization();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
            });

            app.UseSwagger();
            app.UseSwaggerUI(c =>
            {
                c.SwaggerEndpoint("/swagger/v1/swagger.json", "MovieDbAPI");
            });
        }
    }
}
