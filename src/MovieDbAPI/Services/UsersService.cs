﻿namespace MovieDbAPI.Services
{
    using System;
    using System.Threading.Tasks;

    using Microsoft.Extensions.Logging;

    using MovieDbAPI.Data;
    using MovieDbAPI.Data.DbModels;
    using MovieDbAPI.Models.HttpModels.Requests;
    using MovieDbAPI.Models.HttpModels.Responses;

    public class UsersService
    {
        private readonly ILogger<UsersService> logger;
        private readonly ApplicationDbContext dbContext;

        public UsersService(ILogger<UsersService> log, ApplicationDbContext dbContext)
        {
            this.logger = log;
            this.dbContext = dbContext;
        }

        public async Task<BaseResponse> RegisterUser(RegisterRequest request)
        {
            try
            {
                User user = new User() {
                    Email = request.Email,
                    Username = request.Username,
                    Password = request.Password
                };

                await this.dbContext.AddAsync(user);
                await this.dbContext.SaveChangesAsync();

                return new BaseResponse() { IsSuccessfull = true };
            }
            catch (Exception ex)
            {
                this.logger.LogInformation($"Error while creating user in DB. Message: {ex.Message} Stack: {ex.StackTrace} Ex: {ex}");
                throw ex;
            }
        }

        public async Task<BaseResponse> LoginUser(LoginRequest request)
        {
            var resposnse = new BaseResponse();

            try
            {
                // login logic
                await this.dbContext.SaveChangesAsync();
                return resposnse;
            }
            catch (Exception ex)
            {
                this.logger.LogInformation($"Error while login user from DB. Message: {ex.Message} Stack: {ex.StackTrace} Ex: {ex}");
                throw ex;
            }
        }
    }
}
