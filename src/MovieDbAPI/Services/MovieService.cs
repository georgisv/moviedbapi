﻿namespace MovieDbAPI.Services
{
    using System;
    using System.Net.Http;
    using System.Threading.Tasks;

    using Microsoft.Extensions.Logging;

    using MovieDbAPI.Models.ConfigModels;

    public class MovieService
    {
        private readonly ILogger<MovieService> logger;

        private readonly MovieDbConfig movieDbConfig;
        private readonly HttpClient client;

        public MovieService(ILogger<MovieService> log, IHttpClientFactory httpClientFactory, MovieDbConfig movieDbConfig)
        {
            this.logger = log;
            this.movieDbConfig = movieDbConfig;
            this.client = httpClientFactory.CreateClient("MovieDb");
        }

        /// <summary>
        /// Get current populat movies by MoviesDb
        /// </summary>
        /// <returns></returns>
        public async Task GetPopularMovies()
        {
            try
            {
                var res = await this.client.GetAsync($"movie/popular?api_key={this.movieDbConfig.ApiKey}");
                var responseAsString = await res.Content.ReadAsStringAsync();
                // Add logic / Do someting with the data
            }
            catch (Exception ex)
            {
                this.logger.LogError($"Error while getting popular movies! Message: {ex.Message} Stack: {ex.StackTrace} Ex: {ex}");
                throw ex;
            }
        }

        /// <summary>
        /// Get trending movies for different periods - day, week, month and etc.
        /// </summary>
        /// <param name="periodType"></param>
        /// <returns></returns>
        public async Task GetTrendingMoviesBy(string periodType)
        {
            try
            {
                await this.client.GetAsync($"trending/movie/{periodType}?api_key={this.movieDbConfig.ApiKey}");
            }
            catch (Exception ex)
            {
                this.logger.LogError($"Error while getting trending movies! Message: {ex.Message} Stack: {ex.StackTrace} Ex: {ex}");
                throw ex;
            }
        }
    }
}
