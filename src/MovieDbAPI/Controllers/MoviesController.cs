﻿namespace MovieDbAPI.Controllers
{
    using Microsoft.AspNetCore.Mvc;
    using Microsoft.Extensions.Logging;

    using MovieDbAPI.Services;
    using System;
    using System.Threading.Tasks;

    [ApiController]
    [Route("[controller]/[action]")]
    public class MoviesController : ControllerBase
    {
        private readonly ILogger<MoviesController> logger;
        private readonly MovieService movieService;

        public MoviesController(ILogger<MoviesController> log, MovieService movieService)
        {
            this.logger = log;
            this.movieService = movieService;
        }

        [HttpGet]
        public async Task GetPopularMovies()
        {
            try
            {
                await this.movieService.GetPopularMovies();
            }
            catch (Exception ex)
            {

                this.logger.LogError($"Error in MoviesController! Message: {ex.Message} Stack: {ex.StackTrace} Ex: {ex}");
            }
        }
    }
}
