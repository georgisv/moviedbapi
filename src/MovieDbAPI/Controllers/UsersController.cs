﻿namespace MovieDbAPI.Controllers
{
    using System;
    using System.ComponentModel.DataAnnotations;
    using System.Threading.Tasks;

    using Microsoft.AspNetCore.Mvc;
    using Microsoft.Extensions.Logging;

    using MovieDbAPI.Models.HttpModels.Requests;
    using MovieDbAPI.Models.HttpModels.Responses;
    using MovieDbAPI.Services;

    [ApiController]
    [Route("[controller]/[action]")]
    public class UsersController : ControllerBase
    {
        private readonly ILogger<UsersController> logger;
        private readonly UsersService userService;

        public UsersController(ILogger<UsersController> log, UsersService userService)
        {
            this.logger = log;
            this.userService = userService;
        }

        [HttpPost]
        public async Task<BaseResponse> Register(RegisterRequest request)
        {
            var resposnse = new BaseResponse();

            try
            {
                if (string.IsNullOrEmpty(request.Email) || string.IsNullOrEmpty(request.Username) || string.IsNullOrEmpty(request.Password))
                {
                    resposnse.IsSuccessfull = false;
                    resposnse.ErrorMessage = "Missing fields - email, username or password!";
                    return resposnse;
                }

                if (!(new EmailAddressAttribute().IsValid(request.Email)))
                {
                    resposnse.IsSuccessfull = false;
                    resposnse.ErrorMessage = "Invalid Email address!";
                    return resposnse;
                }

                if (request.Password != request.RepeatedPassword)
                {
                    resposnse.IsSuccessfull = false;
                    resposnse.ErrorMessage = "Passwords did not match!";
                    return resposnse;
                }

                resposnse = await this.userService.RegisterUser(request);
                return resposnse;
            }
            catch (Exception ex)
            {
                this.logger.LogError($"Error during registration! Message: {ex.Message} Stack: {ex.StackTrace} Ex: {ex}");
                resposnse.IsSuccessfull = false;
                resposnse.ErrorMessage = "Error on processing request!";

                return resposnse;
            }
        }

        [HttpPost]
        public async Task<BaseResponse> Login(LoginRequest request)
        {
            var response = new BaseResponse();

            try
            {
                if (string.IsNullOrEmpty(request.Email) || string.IsNullOrEmpty(request.Password))
                {
                    response.IsSuccessfull = false;
                    response.ErrorMessage = "Missing fields - email or password!";
                    return response;
                }

                response = await this.userService.LoginUser(request);
                return response;
            }
            catch (Exception ex)
            {
                this.logger.LogError($"Error during login! Message: {ex.Message} Stack: {ex.StackTrace} Ex: {ex}");
                response.IsSuccessfull = false;
                response.ErrorMessage = "Error on processing request";

                return response;
            }
        }

        [HttpPost]
        public bool LogOut()
        {
            return true;
        }
    }
}
