﻿namespace MovieDbAPI.Models.ConfigModels
{
    public class MovieDbConfig
    {
        public string ApiKey { get; set; }

        public string BaseUrl { get; set; }

        public string BasePictureUrl { get; set; }
    }
}
