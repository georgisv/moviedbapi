﻿namespace MovieDbAPI.Models.HttpModels.Responses
{
    public class BaseResponse
    {
        public string ErrorMessage { get; set; }

        public bool IsSuccessfull { get; set; }
    }
}
