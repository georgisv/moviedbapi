﻿namespace MovieDbAPI.Models.HttpModels.Requests
{
    public class LoginRequest
    {
        public string Email { get; set; }

        public string Password { get; set; }
    }
}
