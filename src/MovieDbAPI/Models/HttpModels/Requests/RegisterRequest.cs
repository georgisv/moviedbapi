﻿namespace MovieDbAPI.Models.HttpModels.Requests
{
    public class RegisterRequest : LoginRequest
    {
        public string Username { get; set; }

        public string RepeatedPassword { get; set; }
    }
}
